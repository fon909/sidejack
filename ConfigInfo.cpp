/*
 * ConfigInfo.cpp
 *
 *  Created on: Aug 16, 2011
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <sstream>
#include <fstream>
using namespace std;

#include "ConfigInfo.hpp"

bool exec(char const* cmd, string &result) {
    FILE* pipe = popen(cmd, "r");
    if (!pipe) return false;
    char buffer[128];
    result = "";

    while(!feof(pipe)) {
        if(fgets(buffer, 128, pipe) != NULL)
                result += buffer;
    }
    pclose(pipe);
    return true;
}

UserInfo *ConfigInfo::UpdateDatabase(HttpPacket *packet) {
	map<string, string> m;
	ParseCookie(packet->cookies(), m);
	set<string>::iterator it;
	map<string,string>::iterator mit;
	if(IsCookieComplete(m)) {
		// map to string
		string new_cookie = "";
		for(it = cookie.begin(); it!=cookie.end(); ++it) {
			mit = m.find(*it);
			new_cookie += mit->first;
			new_cookie += "=";
			new_cookie += mit->second;
			new_cookie += " ";
		}
		if(cookie_users.find(new_cookie) == cookie_users.end()) {
			cookie_users[new_cookie] = UserInfo(new_cookie, packet->user_agent());
			UserInfo *puser = &(cookie_users[new_cookie]);
			if(user=="") return puser;
			string cmd, result;
			if(UpdateUserInfo(*puser, cmd, result))
				return puser;
			else {
#ifdef VERBOSE
				ostringstream oss;
				oss << "tmp-" << time(0) << ".html";
				ofstream fout(oss.str().c_str());
				fout << result;
				fout.close();
				cerr << "[ !! ] Failed to retrieve user info: " << packet->from() << " --> " << packet->to() << ", Host: "
					 << packet->host() << "; "
					 << "Result saved in temp file \"" << oss.str() << "\"\n";
#endif
				return 0;
			}
		}
	}
	return 0;
}

bool ConfigInfo::Load1(ifstream &fin) {
	static string const CHUNK_BEGIN = "{", CHUNK_END = "}";
	string tmp;
	while(fin >> tmp) {
		if(tmp == CHUNK_BEGIN) {
			while(fin >> tmp) {
				if(tmp == CHUNK_END) break;
				if(tmp == "Host:") {fin >> host;}
				if(tmp == "Url:") {fin >> url;}
				else if(tmp == "Cookie:") {
					getline(fin, tmp);
					istringstream iss(tmp);
					while(iss >> tmp) {
						cookie.insert(tmp);
					}
				}
				else if(tmp == "User:") { fin >> user; }
				else if(tmp == "User-Begin:") { fin >> user_begin; }
				else if(tmp == "User-End:") { fin >> user_end; }
				else if(tmp == "Avatar:") { fin >> avatar; }
				else if(tmp == "Avatar-Begin:") { fin >> avatar_begin; }
				else if(tmp == "Avatar-End:") { fin >> avatar_end; }
				else if(tmp == "Comment:") { getline(fin, tmp); }
			}
			break;
		}
	}
	return fin;
}

bool ConfigInfo::UpdateUserInfo(UserInfo &user_info, string &cmd, string &result) {

	cmd = "curl -s --user-agent \"";
	cmd += user_info.GetAgent();
	cmd += "\" --cookie \"";
	ostringstream oss;
	if(user_info.GetCookie().find('!')!=string::npos) { // use file version
		oss << ".cookie-tmp-" << time(0);
		ofstream fout(oss.str().c_str());
		fout << user_info.GetCookie();
		fout.close();
		cmd += oss.str();
	}
	else cmd += user_info.GetCookie();
	cmd += "\" ";
	cmd += url;
	cout << "cmd = " << cmd << endl;
	bool bs = exec(cmd.c_str(), result);
	ostringstream oss2;
	oss2 << "rm " << oss.str();
	int r = system(oss2.str().c_str()); r++; // r is for ease the warning

	while(bs) { // parse the result
		size_t pos, pos_end;
		pos = result.find(user);
		if(pos == string::npos) break;
		pos = result.find(user_begin, pos + user.length()) + user_begin.length();
		if(pos == string::npos) break;
		pos_end = result.find(user_end, pos);
		if(pos == string::npos || pos_end <= pos) break;
		user_info.SetName(result.substr(pos, pos_end - pos));

		if(avatar != "") {
			pos = result.find(avatar);
			if(pos == string::npos) break;
			pos = result.find(avatar_begin, pos + avatar.length()) + avatar_begin.length();
			if(pos == string::npos) break;
			pos_end = result.find(avatar_end, pos);
			if(pos == string::npos || pos_end <= pos) break;
			user_info.SetAvatar(result.substr(pos, pos_end - pos));
		}
		return true;
	}
	return false;
}

void ConfigInfo::ParseCookie(string const &cookie, map<string,string> &m) {
	istringstream iss(cookie);
	string name, value;
	size_t pos;
	while(iss >> name) {
		pos = name.find('=');
		if(pos == string::npos) break;
		value = name.substr(pos+1);
		if(value[value.size()-1] != ';') value += ';';
		name = name.substr(0, pos);
		m[name] = value;
	}
}

bool ConfigInfo::IsCookieComplete(map<string, string> &m) {
	if(!cookie.empty()) {
		for(set<string>::const_iterator cit = cookie.begin();
			cit != cookie.end();
			++cit) {
			if(m.find(*cit) == m.end()) {
				return false;
			}
		}
	}
	return true;
}
