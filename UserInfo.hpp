/*
 * UserInfo.hpp
 *
 *  Created on: Aug 16, 2011
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef USERINFO_HPP_
#define USERINFO_HPP_

#include <string>
using namespace std;

class UserInfo {
public:
	UserInfo(string const &cookie="", string const &agent="",
			 string const &name="", string const &avatar="")
		: user_cookie(cookie), user_agent(agent), user_name(name), user_avatar(avatar) {}

	string const &GetCookie() const {return user_cookie;}
	string const &GetAgent() const {return user_agent;}
	string const &GetName() const {return user_name;}
	string const &GetAvatar() const {return user_avatar;}
	void SetCookie(string const &cookie) {user_cookie = cookie;}
	void SetAgent(string const &agent) {user_agent = agent;}
	void SetName(string const &name) {user_name = name;}
	void SetAvatar(string const &avatar) {user_avatar = avatar;}

private:
	string user_cookie, user_agent, user_name, user_avatar;
};

#endif /* USERINFO_HPP_ */
