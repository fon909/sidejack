### 簡介 ###

Sidejack 是可以發動連線劫持 (Session Hijacking) 的程式，官方網站[在此](http://securityalley.blogspot.tw/2014/11/sidejack.html)。

Sidejack 是受 Firesheep 的啟發，雖然一直知道 Session Hijack，但是直到看到 Firesheep 才想說，不如我也來寫一個類似的工具程式好了，所以就 Sidejack 就這樣在兩天之後誕生了。

寫成 Firefox 的外掛需要寫 Javascript 程式，我的 Javascript 不熟，所以我不想寫成外掛，想把它作成一個獨立的程式，如果你對於寫 Firefox 外掛很有興趣，我們可以合作把 Sidejack 移植到火狐狸上，請留言給我。

Sidejack 是純粹用 C++ 寫的，程式碼內包含了 Ryan Dahl 的 http-parser，這部份是 C 語法，以及 Firesheep 的 http sniffer，這部份是很漂亮的 C++。

這裡是它在 BitBucket 的網頁。

Sidejack 使用 GPL。