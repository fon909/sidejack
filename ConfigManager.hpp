/*
 * ConfigManager.hpp
 *
 *  Created on: Aug 16, 2011
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef CONFIGMANAGER_HPP_
#define CONFIGMANAGER_HPP_

#include <string>
#include <map>
using namespace std;

#include "http_packet.hpp"

#include "UserInfo.hpp"
#include "ConfigInfo.hpp"
#include "ConfigManager.hpp"

class ConfigManager {
public:
	bool Init(char const *filename);
	UserInfo *UpdateDatabase(HttpPacket *packet);
public:
	map<string, ConfigInfo> configs;
};

#endif /* CONFIGMANAGER_HPP_ */
