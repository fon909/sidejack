/*
 * ConfigManager.cpp
 *
 *  Created on: Aug 16, 2011
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <vector>
using namespace std;

#include "ConfigManager.hpp"

ConfigManager Gm;

bool ConfigManager::Init(char const *filename) {
	ifstream fin(filename);
	if(!fin) return false;

	while(fin) {
		ConfigInfo info;
		if(info.Load1(fin)) {
			if(info.host != "") configs[info.host] = info;
		}
	}
	return configs.size()>0;
}

UserInfo *ConfigManager::UpdateDatabase(HttpPacket *packet) {
	map<string, ConfigInfo>::iterator it;
	string host = packet->host(), host2, host3;
	vector<string> v;
	size_t pos = 0, pos_end = host.find('.');

	while(pos_end != string::npos) {
		v.push_back(host.substr(pos, pos_end-pos));
		pos = pos_end + 1;
		pos_end = host.find('.', pos);
	}
	v.push_back(host.substr(pos));
	if(v.size() >= 2) host2 = v[v.size()-2] + "." + v[v.size()-1];
	if(v.size() >= 3) host3 = v[v.size()-3] + "." + v[v.size()-2] + "." + v[v.size()-1];

	bool bfind = false;
	if((it=configs.find(host)) != configs.end()) bfind = true;
	if(!bfind && host3 != "" && (it=configs.find(host3)) != configs.end()) bfind = true;
	if(!bfind && host2 != "" && (it=configs.find(host2)) != configs.end()) bfind = true;

	if(bfind) {
		ConfigInfo &info = it->second;
		return info.UpdateDatabase(packet);
	}
	else return 0;
}
