################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../ConfigInfo.cpp \
../ConfigManager.cpp \
../http_packet.cpp \
../http_sniffer.cpp \
../main.cpp 

C_SRCS += \
../http_parser.c 

OBJS += \
./ConfigInfo.o \
./ConfigManager.o \
./http_packet.o \
./http_parser.o \
./http_sniffer.o \
./main.o 

C_DEPS += \
./http_parser.d 

CPP_DEPS += \
./ConfigInfo.d \
./ConfigManager.d \
./http_packet.d \
./http_sniffer.d \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DDEBUG -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


