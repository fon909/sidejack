/*
 * ConfigInfo.hpp
 *
 *  Created on: Aug 16, 2011
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef CONFIGINFO_HPP_
#define CONFIGINFO_HPP_

#include <fstream>
#include <string>
#include <map>
#include <set>
using namespace std;

#include "http_packet.hpp"
#include "UserInfo.hpp"

class ConfigInfo {
friend class ConfigManager;
private:
	UserInfo *UpdateDatabase(HttpPacket *packet);
	bool Load1(ifstream &fin);
	bool UpdateUserInfo(UserInfo &user_info, string &cmd, string &result);
	void ParseCookie(string const &cookie, map<string,string> &m);
	bool IsCookieComplete(map<string, string> &m);

private:
	string host, url, user, user_begin, user_end, avatar, avatar_begin, avatar_end;
	set<string> cookie;
	map<string, UserInfo> cookie_users;
};



#endif /* CONFIGINFO_HPP_ */
